Goto [the wiki](https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home). And remember to complain that

* this is not a project-creation option
* one must apparently give a fully-qualified link to the wiki (both my attempts at [filepath-relative links fail](https://bitbucket.org/site/master/issue/6315/relative-urls-in-readmemd-files-only-work)ed)
