THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
# for manual testing
#THIS_FN='OpenVPN_config_client.sh'
MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### `source` code presumed to be in same dir/folder.

source ${THIS_DIR}/private.properties
source ${THIS_DIR}/my_linode_bash_library.sh

### ASSERT: client packages previously installed

### Copy client configuration and credentials files previously generated on server.
### (Both necessary per `the Debian howto <https://wiki.debian.org/openvpn%20for%20server%20and%20client>`_.)
### Note this can be undone (e.g., for cleanup) with ./OpenVPN_config_client_undo.sh

# Note following defined in private.properties:
# * MYNODE_USER_NAME
# * MYNODE_IPV4
# * OPENVPN_CLIENT_NAME
# * OPENVPN_CLIENT_STASH_DIR
# * OPENVPN_CLIENT_TARGET_DIR
# TODO: test -z !

## Copy client config and credentials to $HOME (will later move)
## ASSERT: on server: credentials for CLIENT_NAME have been copied to $HOME for username
## TODO: use parameterized username
## TODO: test existence of file first? now that we have visiblepw
for CLIENT_CONFIG_FN in \
  'ca.crt' \
  "${OPENVPN_CLIENT_NAME}.conf" \
  "${OPENVPN_CLIENT_NAME}.crt" \
  "${OPENVPN_CLIENT_NAME}.key" \
; do
  CLIENT_CONFIG_FP="${OPENVPN_CLIENT_STASH_DIR}/${CLIENT_CONFIG_FN}"
  if [[ -r "${CLIENT_CONFIG_FP}" ]] ; then
    echo -e "${WARNING_PREFIX} local file='${CLIENT_CONFIG_FP}' exists, will not be replaced"
  else
    # file we want we expect to be in ${HOME} for ${MYNODE_USER_NAME} on ${MYNODE_IPV4}
    SERVER_CLIENT_CONFIG_FP="${MYNODE_USER_NAME}@${MYNODE_IPV4}:${CLIENT_CONFIG_FN}"
    for CMD in \
      "scp ${SERVER_CLIENT_CONFIG_FP} ${CLIENT_CONFIG_FP}" \
      "ls -al ${CLIENT_CONFIG_FP}" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}"
      eval "${CMD}"
    done
    echo # newline
  fi
#  if [[ ! -r "${CLIENT_CONFIG_FP}" ]] ; then
  # Gotta test with '-e' since some of these files are owner=root permissions=600
  if [[ ! -e "${CLIENT_CONFIG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} file='${CLIENT_CONFIG_FP}' does not exist, exiting ..." 1>&2 # stderr
    exit 2
  fi
done

# Move client config and credentials to /etc/openvpn/
for CLIENT_CONFIG_FN in \
  'ca.crt' \
  "${OPENVPN_CLIENT_NAME}.conf" \
  "${OPENVPN_CLIENT_NAME}.crt" \
  "${OPENVPN_CLIENT_NAME}.key" \
; do
  CLIENT_SOURCE_FP="${OPENVPN_CLIENT_STASH_DIR}/${CLIENT_CONFIG_FN}"
  CLIENT_TARGET_FP="${OPENVPN_CLIENT_TARGET_DIR}/${CLIENT_CONFIG_FN}"
  if [[ -e "${CLIENT_TARGET_FP}" ]] ; then
    echo -e "${WARNING_PREFIX} local file='${CLIENT_TARGET_FP}' exists, will not be replaced"
  else
    for CMD in \
      "sudo mv ${CLIENT_SOURCE_FP} ${CLIENT_TARGET_FP}" \
      "sudo chown root:root ${CLIENT_TARGET_FP}" \
      "sudo ls -al ${CLIENT_TARGET_FP}" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}"
      eval "${CMD}"
    done
    echo # newline
  fi
  if [[ ! -e "${CLIENT_TARGET_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} file='${CLIENT_TARGET_FP}' does not exist, exiting ..." 1>&2 # stderr
    exit 3
  fi
done

# > -rw-r--r-- 1 root root 1387 Nov  2 19:47 /etc/openvpn/ca.crt
# > -rw-r--r-- 1 root root  371 Nov  2 19:47 /etc/openvpn/client1.conf
# > -rw-r--r-- 1 root root 4011 Nov  2 19:47 /etc/openvpn/client1.crt
# > -rw------- 1 root root  916 Nov  2 19:47 /etc/openvpn/client1.key

### Configure client ``/etc/default/openvpn`` to *not* autostart.

FP='/etc/default/openvpn'
VAR='AUTOSTART="none"'
fgrep -nH -e ${VAR} ${FP}
sudo_backup ${FP}

# TODO: don't rely on this particular style of commenting!
sudo sed -i -e "s/^#${VAR}/${VAR}/" ${FP}
fgrep -nH -e ${VAR} ${FP}

exit 0
