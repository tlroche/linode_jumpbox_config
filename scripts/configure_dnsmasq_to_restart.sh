### From https://www.linode.com/docs/networking/vpn/secure-communications-with-openvpn-on-ubuntu-12-04-precise-and-debian-7#tunneling-all-connections-through-the-vpn :
### > [On reboot], dnsmasq will try to start before the OpenVPN tun device has been enabled[, causing] dnsmasq to fail[.]
### > To rectify this, modify your /etc/rc.local file [to] restart dnsmasq after [the other] init scripts have finished.
### > [Place this] restart command after your iptables rules[, and end /etc/rc.local with `exit 0`]

/etc/init.d/dnsmasq restart
