#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Get "local routes": routes present on the client (not the linode, but the box from which you are `ssh`ing to the linode) *before* running OpenVPN or F5VPN clients.
### From them, parse some information from them for future use. Accordingly, this will probably be `source`d.
### Requires:
### * bash new enough to support all the string-manipulation woo-hoo
### * basename, dirname
### * fgrep
### * `ip` tools including `ip route`
### * readlink

### Requires path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
### Presuming you checked out the project to "${HOME}/code/bash/linode/linode_jumpbox_config",
### you could pass the path like:
### > DIR="${HOME}/code/bash/linode/linode_jumpbox_config/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" source "${DIR}/get_local_routes.sh"

### TODO:
### * ensure we have no running clients={OpenVPN, F5VPN}

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}=='-bash'

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='get_local_routes.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

### other needed constants must be set in *.properties
### TODO (for this and get_*.sh)? `source` and `export` in driver?

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### parse OpenVPN routes
### ----------------------------------------------------------------------

### Note following is heavily dependent on `ip addr` grammar

### 'local' == client LAN, 'IPN' == (IPv4) IP "number" ("dotted-quad" string)
### ${LOCAL_NETWORK_LINK} from public.properties
if [[ -z "${LOCAL_NETWORK_LINK}" ]] ; then
  echo -e "${ERROR_PREFIX} LOCAL_NETWORK_LINK not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 4
  fi
else

  ## get local IP#
  LOCAL_IPN_RAW1="$(ip addr show ${LOCAL_NETWORK_LINK} | fgrep -e 'inet ')" # local var
  LOCAL_IPN_RAW2="${LOCAL_IPN_RAW1%%/*}"                                         # ditto
#  echo -e "LOCAL_IPN_RAW2='${LOCAL_IPN_RAW2}'"
  LOCAL_IPN_RAW3="${LOCAL_IPN_RAW2##*inet }"
  LOCAL_IPN="$(echo -e ${LOCAL_IPN_RAW3} | tr -d '[[:space:]]')"
  if [[ -z "${LOCAL_IPN}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to get local/LAN IP# from local interface, exiting ..." 1>&2
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 5
    fi
  else # success!
    echo -e "${MESSAGE_PREFIX} local/LAN IP#='${LOCAL_IPN}'"
  fi

  ## get local routes for later use
  ## TODO: test that each is a *single* route/line && !null
  LOCAL_DEFAULT_ROUTE_RAW="$(ip route show | fgrep -e 'default via')" # local var
  # annoyingly, must trim space
  LOCAL_DEFAULT_ROUTE="$(echo -e "${LOCAL_DEFAULT_ROUTE_RAW}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
  if [[ -z "${LOCAL_DEFAULT_ROUTE}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to get local default route, exiting ..." 1>&2 # stderr
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 6
    fi
  else # success!
    echo -e "${MESSAGE_PREFIX} local default route='${LOCAL_DEFAULT_ROUTE}'"
  fi

  LOCAL_IPN_ROUTE_RAW="$(ip route show | fgrep -e "${LOCAL_IPN}")"    # local var
  LOCAL_IPN_ROUTE="$(echo -e "${LOCAL_IPN_ROUTE_RAW}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
  if [[ -z "${LOCAL_IPN_ROUTE}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to get route from local IP#, exiting ..." 1>&2 # stderr
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 7
    fi
  else # success!
    echo -e "${MESSAGE_PREFIX} route from local IP#='${LOCAL_IPN_ROUTE}'"
  fi

fi # [[ -z "${LOCAL_NETWORK_LINK}" ]]

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
