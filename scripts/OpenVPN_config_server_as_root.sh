### just for logging

THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
# for manual testing
#THIS_FN='OpenVPN_config_server_as_root.sh'
MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

source ~/private.properties        # previously copied
source ~/my_linode_bash_library.sh # ditto

ls -al /etc/openvpn/easy-rsa/2.0/keys/
# > ls: cannot access /etc/openvpn/easy-rsa/2.0/keys/: No such file or directory

pushd /etc/openvpn/easy-rsa/2.0/
. ./vars
# > ls: cannot access /etc/openvpn/easy-rsa/2.0/keys/: No such file or directory
. ./clean-all

# . ./build-ca
# hit RETx8 (or '.'x8, per `build-ca`) to take previously-set defaults
. ./build-ca << 'END_HEREDOC'








END_HEREDOC
ls -al /etc/openvpn/easy-rsa/2.0/keys/
# > -rw-r--r-- 1 root root 1387 Oct  2 20:52 ca.crt
# > -rw------- 1 root root  916 Oct  2 20:52 ca.key
# > -rw-r--r-- 1 root root    0 Oct  2 20:51 index.txt
# > -rw-r--r-- 1 root root    3 Oct  2 20:51 serial

### Generate private keys with build-key-server

# could not make following work as heredoc! (see above)
# so instead just make `build-key-server` take the defaults :-(
# TODO: set 'extra' attributes={challenge password, optional company name}
FP='/etc/openvpn/easy-rsa/2.0/build-key-server'
DIR="$(dirname ${FP})"
FN="$(basename ${FP})"
backup ${FP}
sed -i -e 's|--interact||' ${FP}
. ./${FN} server
ls -al /etc/openvpn/easy-rsa/2.0/keys/
# > -rw-r--r-- 1 root root 4129 Oct  2 21:56 01.pem
# > -rw-r--r-- 1 root root 1387 Oct  2 21:55 ca.crt
# > -rw------- 1 root root  912 Oct  2 21:55 ca.key
# > -rw-r--r-- 1 root root  139 Oct  2 21:56 index.txt
# > -rw-r--r-- 1 root root   21 Oct  2 21:56 index.txt.attr
# > -rw-r--r-- 1 root root    0 Oct  2 21:55 index.txt.old
# > -rw-r--r-- 1 root root    3 Oct  2 21:56 serial
# > -rw-r--r-- 1 root root    3 Oct  2 21:55 serial.old
# > -rw-r--r-- 1 root root 4129 Oct  2 21:56 server.crt
# > -rw-r--r-- 1 root root  733 Oct  2 21:56 server.csr
# > -rw------- 1 root root  916 Oct  2 21:56 server.key

### Generate credentials (e.g., certificates, keys) for all clients (only one for now)

## Zero-out' index.txt before generating client credentials. (TODO: find out why this is necessary)

# pushd /etc/openvpn/easy-rsa/2.0/keys/
mv keys/index.txt keys/index.txt.1
mv keys/index.txt.old keys/index.txt
# popd
ls -al /etc/openvpn/easy-rsa/2.0/keys/
# > -rw-r--r-- 1 root root 4129 Oct  2 21:56 01.pem
# > -rw-r--r-- 1 root root 1387 Oct  2 21:55 ca.crt
# > -rw------- 1 root root  912 Oct  2 21:55 ca.key
# > -rw-r--r-- 1 root root    0 Oct  2 21:55 index.txt
# > -rw-r--r-- 1 root root  139 Oct  2 21:56 index.txt.1
# > -rw-r--r-- 1 root root   21 Oct  2 21:56 index.txt.attr
# > -rw-r--r-- 1 root root    3 Oct  2 21:56 serial
# > -rw-r--r-- 1 root root    3 Oct  2 21:55 serial.old
# > -rw-r--r-- 1 root root 4129 Oct  2 21:56 server.crt
# > -rw-r--r-- 1 root root  733 Oct  2 21:56 server.csr
# > -rw------- 1 root root  916 Oct  2 21:56 server.key

## Generate client (only one for now) credentials.

# OPENVPN_CLIENT_NAME defined in private.properties
FP='/etc/openvpn/easy-rsa/2.0/build-key'
DIR="$(dirname ${FP})"
FN="$(basename ${FP})"
backup ${FP}

# try generating client cert with heredoc? first, just make it take defaults :-(
# TODO: set 'extra' attributes={challenge password, optional company name}
pushd ${DIR}
sed -i -e 's|--interact||' ./${FN}
. ./${FN} ${OPENVPN_CLIENT_NAME}
# produces output like
# > Using Common Name: TomRoche
# > Generating a 1024 bit RSA private key
# > ...++++++
# > .....................++++++
# > writing new private key to 'client1.key'
# > -----
# > Using configuration from /etc/openvpn/easy-rsa/2.0/openssl.cnf
# > Check that the request matches the signature
# > Signature ok
# > The Subject's Distinguished Name is as follows
# > countryName           :PRINTABLE:'US'
# > stateOrProvinceName   :PRINTABLE:'NC'
# > localityName          :PRINTABLE:'Carrboro'
# > organizationName      :PRINTABLE:'AQMEII-NA-N2O'
# > organizationalUnitName:PRINTABLE:'TomRoche'
# > commonName            :PRINTABLE:'TomRoche'
# > name                  :PRINTABLE:'Tom Roche'
# > emailAddress          :IA5STRING:'Tom_Roche@pobox.com'
# > Certificate is to be certified until Oct  7 01:08:32 2024 GMT (3650 days)
# >
# > Write out database with 1 new entries
# > Data Base Updated

ls -al /etc/openvpn/easy-rsa/2.0/keys/
# > -rw-r--r-- 1 root root 4129 Nov  1 21:03 01.pem
# > -rw-r--r-- 1 root root 4011 Nov  1 21:38 02.pem
# > -rw-r--r-- 1 root root 1387 Nov  1 21:02 ca.crt
# > -rw------- 1 root root  916 Nov  1 21:02 ca.key
# > -rw-r--r-- 1 root root 4011 Nov  1 21:38 client1.crt
# > -rw-r--r-- 1 root root  733 Nov  1 21:38 client1.csr
# > -rw------- 1 root root  920 Nov  1 21:38 client1.key
# > -rw-r--r-- 1 root root  139 Nov  1 21:38 index.txt
# > -rw-r--r-- 1 root root  139 Nov  1 21:03 index.txt.1
# > -rw-r--r-- 1 root root   21 Nov  1 21:38 index.txt.attr
# > -rw-r--r-- 1 root root   21 Nov  1 21:03 index.txt.attr.old
# > -rw-r--r-- 1 root root    0 Nov  1 21:01 index.txt.old
# > -rw-r--r-- 1 root root    3 Nov  1 21:38 serial
# > -rw-r--r-- 1 root root    3 Nov  1 21:03 serial.old
# > -rw-r--r-- 1 root root 4129 Nov  1 21:03 server.crt
# > -rw-r--r-- 1 root root  733 Nov  1 21:03 server.csr
# > -rw------- 1 root root  916 Nov  1 21:03 server.key

### Copy client credentials to matching userspace (for subsequent retrieval)

# need not run as root, but will "since we're here."
# TODO: test dirs
SDIR='/etc/openvpn/easy-rsa/2.0/keys'  # source
TDIR="${OPENVPN_CLIENT_USER_HOME}"     # target
for FN in \
  'ca.crt' \
  "${OPENVPN_CLIENT_NAME}.crt" \
  "${OPENVPN_CLIENT_NAME}.key" \
; do
  # TODO: test files
  SFP="${SDIR}/${FN}"
  TFP="${TDIR}/${FN}"

  for CMD in \
    "ls -al ${SFP}" \
    "cp ${SFP} ${TFP}" \
    "chown tlroche:tlroche ${TFP}" \
    "ls -al ${TFP}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
  done
  echo # newline
done

### Generate Diffie-Hellman parameters. Also has the following unfortunate constraints for scripting:

FP='/etc/openvpn/easy-rsa/2.0/build-dh' # fortunately no editing required
DIR="$(dirname ${FP})"
FN="$(basename ${FP})"

ls -al ${DIR}/keys
# > -rw-r--r-- 1 root root 4129 Nov  1 21:03 01.pem
# > -rw-r--r-- 1 root root 4011 Nov  1 21:38 02.pem
# > -rw-r--r-- 1 root root 1387 Nov  1 21:02 ca.crt
# > -rw------- 1 root root  916 Nov  1 21:02 ca.key
# > -rw-r--r-- 1 root root 4011 Nov  1 21:38 client1.crt
# > -rw-r--r-- 1 root root  733 Nov  1 21:38 client1.csr
# > -rw------- 1 root root  920 Nov  1 21:38 client1.key
# > -rw-r--r-- 1 root root  139 Nov  1 21:38 index.txt
# > -rw-r--r-- 1 root root  139 Nov  1 21:03 index.txt.1
# > -rw-r--r-- 1 root root   21 Nov  1 21:38 index.txt.attr
# > -rw-r--r-- 1 root root   21 Nov  1 21:03 index.txt.attr.old
# > -rw-r--r-- 1 root root    0 Nov  1 21:01 index.txt.old
# > -rw-r--r-- 1 root root    3 Nov  1 21:38 serial
# > -rw-r--r-- 1 root root    3 Nov  1 21:03 serial.old
# > -rw-r--r-- 1 root root 4129 Nov  1 21:03 server.crt
# > -rw-r--r-- 1 root root  733 Nov  1 21:03 server.csr
# > -rw------- 1 root root  916 Nov  1 21:03 server.key

pushd ${DIR}
. ./build-dh
popd

ls -al ${DIR}/keys
# > -rw-r--r-- 1 root root 4129 Nov  1 21:03 01.pem
# > -rw-r--r-- 1 root root 4011 Nov  1 21:38 02.pem
# > -rw-r--r-- 1 root root 1387 Nov  1 21:02 ca.crt
# > -rw------- 1 root root  916 Nov  1 21:02 ca.key
# > -rw-r--r-- 1 root root 4011 Nov  1 21:38 client1.crt
# > -rw-r--r-- 1 root root  733 Nov  1 21:38 client1.csr
# > -rw------- 1 root root  920 Nov  1 21:38 client1.key
# > -rw-r--r-- 1 root root  245 Nov  1 22:00 dh1024.pem
# > -rw-r--r-- 1 root root  139 Nov  1 21:38 index.txt
# > -rw-r--r-- 1 root root  139 Nov  1 21:03 index.txt.1
# > -rw-r--r-- 1 root root   21 Nov  1 21:38 index.txt.attr
# > -rw-r--r-- 1 root root   21 Nov  1 21:03 index.txt.attr.old
# > -rw-r--r-- 1 root root    0 Nov  1 21:01 index.txt.old
# > -rw-r--r-- 1 root root    3 Nov  1 21:38 serial
# > -rw-r--r-- 1 root root    3 Nov  1 21:03 serial.old
# > -rw-r--r-- 1 root root 4129 Nov  1 21:03 server.crt
# > -rw-r--r-- 1 root root  733 Nov  1 21:03 server.csr
# > -rw------- 1 root root  916 Nov  1 21:03 server.key

### Relocate secure keys. Also has the following unfortunate constraints for scripting:

# TODO: test dirs
SDIR='/etc/openvpn/easy-rsa/2.0/keys'  # source
TDIR='/etc/openvpn'                    # target
for FN in \
  'ca.crt' \
  'ca.key' \
  'dh1024.pem' \
  'server.crt' \
  'server.key' \
; do
  # TODO: test files
  SFP="${SDIR}/${FN}"
  TFP="${TDIR}/${FN}"

  for CMD in \
    "ls -al ${SFP}" \
    "cp ${SFP} ${TFP}" \
    "ls -al ${TFP}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
  done
  echo # newline
done

# > ls -al /etc/openvpn/easy-rsa/2.0/keys/ca.crt
# > -rw-r--r-- 1 root root 1387 Oct  9 21:03 /etc/openvpn/easy-rsa/2.0/keys/ca.crt
# > cp /etc/openvpn/easy-rsa/2.0/keys/ca.crt /etc/openvpn/ca.crt
# > ls -al /etc/openvpn/ca.crt
# > -rw-r--r-- 1 root root 1387 Oct  9 21:11 /etc/openvpn/ca.crt

# > ls -al /etc/openvpn/easy-rsa/2.0/keys/ca.key
# > -rw------- 1 root root 916 Oct  9 21:03 /etc/openvpn/easy-rsa/2.0/keys/ca.key
# > cp /etc/openvpn/easy-rsa/2.0/keys/ca.key /etc/openvpn/ca.key
# > ls -al /etc/openvpn/ca.key
# > -rw------- 1 root root 916 Oct  9 21:11 /etc/openvpn/ca.key

# > ls -al /etc/openvpn/easy-rsa/2.0/keys/dh1024.pem
# > -rw-r--r-- 1 root root 245 Oct  9 21:10 /etc/openvpn/easy-rsa/2.0/keys/dh1024.pem
# > cp /etc/openvpn/easy-rsa/2.0/keys/dh1024.pem /etc/openvpn/dh1024.pem
# > ls -al /etc/openvpn/dh1024.pem
# > -rw-r--r-- 1 root root 245 Oct  9 21:11 /etc/openvpn/dh1024.pem

# > ls -al /etc/openvpn/easy-rsa/2.0/keys/server.crt
# > -rw-r--r-- 1 root root 4129 Oct  9 21:06 /etc/openvpn/easy-rsa/2.0/keys/server.crt
# > cp /etc/openvpn/easy-rsa/2.0/keys/server.crt /etc/openvpn/server.crt
# > ls -al /etc/openvpn/server.crt
# > -rw-r--r-- 1 root root 4129 Oct  9 21:11 /etc/openvpn/server.crt

# > ls -al /etc/openvpn/easy-rsa/2.0/keys/server.key
# > -rw------- 1 root root 916 Oct  9 21:06 /etc/openvpn/easy-rsa/2.0/keys/server.key
# > cp /etc/openvpn/easy-rsa/2.0/keys/server.key /etc/openvpn/server.key
# > ls -al /etc/openvpn/server.key
# > -rw------- 1 root root 916 Oct  9 21:11 /etc/openvpn/server.key
