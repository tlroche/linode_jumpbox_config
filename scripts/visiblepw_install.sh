#!/usr/bin/env bash

### Automates configuration of `sudo` to allow remote `sudo`, e.g., via SSH, by changing 'sudoers' configuration.
### For fuller discussion of the usecase, see http://unix.stackexchange.com/questions/156954/scripting-copy-of-remote-root-owned-files-when-permitrootlogin-no
### As discussed there: when you remote `sudo` using visiblepw, your password will be `echo`ed in the calling shell!

### Assumes
### * user is `sudo`ed on the remote
### * user has previously run minimal_secure_debian-based_linode_config.sh on the remote
### * remote root login is disabled from SSH (otherwise, no need for this usecase!)
### * the usual GNU tools on the remote, including `cat`, `dirname`, `mv`

### TODO:
### * Find a way to avoid password echo
### * Test command error/return codes, exit on failure.
### * Support control flags (e.g., `--dry-run`, `--verbose`) via commandline.
### * Take data constants (below) via commandline.

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

### for logging
THIS="${0}"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
# # for manual testing
# THIS_FN='visiblepw_install.sh'
MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### remote

## path at which to create the 'sudoers' config
VISIBLEPW_FP='/etc/sudoers.d/visiblepw'

## what to write there
VISIBLEPW_PAYLOAD='Defaults  visiblepw # added 15 Sep 2014, attempting to allow remote `sudo`'

## Create a tempfile at a known path (needed in some other usecases) to avoid `sudo`ing a heredoc
## which can get messy--as in, hosing your 'sudoers' and therefore your 'sudo'! been there, done that :-(
VISIBLEPW_TMP_FP="/tmp${VISIBLEPW_FP}"
VISIBLEPW_TMP_DIR="$(dirname ${VISIBLEPW_TMP_FP})"

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

if   [[ -z "${VISIBLEPW_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} VISIBLEPW_FP not defined, exiting ..." 1>&2 # stderr
  exit 2
elif [[ -z "${VISIBLEPW_PAYLOAD}" ]] ; then
  echo -e "${ERROR_PREFIX} VISIBLEPW_PAYLOAD not defined, exiting ..." 1>&2
  exit 3
elif [[ -z "${VISIBLEPW_TMP_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} VISIBLEPW_TMP_FP not defined, exiting ..." 1>&2
  exit 4
elif [[ -z "${VISIBLEPW_TMP_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} VISIBLEPW_TMP_DIR not defined, exiting ..." 1>&2
  exit 5
elif [[ -r "${VISIBLEPW_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} path to file='${VISIBLEPW_FP}' exists: move or delete before proceeding. Exiting ..." 1>&2
  exit 6
elif [[ -r "${VISIBLEPW_TMP_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} path to tempfile='${VISIBLEPW_TMP_FP}' exists: move or delete before proceeding. Exiting ..." 1>&2
  exit 7
else

  ## make temp file, which *should not* require root
  for CMD in \
    "mkdir -p ${VISIBLEPW_TMP_DIR}/" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
  done

  ## write payload to file
  echo -e "${VISIBLEPW_PAYLOAD}" > "${VISIBLEPW_TMP_FP}"

  ## make real file, which *should* require root
  if [[ -r "${VISIBLEPW_TMP_FP}" ]] ; then
    # can't do "my way": `eval` hoses
    sudo mv "${VISIBLEPW_TMP_FP}" "${VISIBLEPW_FP}"
    # note subsequent `sudo` will fail horribly if not owned by root!
    sudo chown root:root "${VISIBLEPW_FP}"
  else
    echo -e "${ERROR_PREFIX} could not write tempfile='${VISIBLEPW_TMP_FP}', exiting ..." 1>&2 # stderr
    exit 8
  fi

  if [[ -r "${VISIBLEPW_FP}" ]] ; then

      for CMD in \
        "ls -al ${VISIBLEPW_FP}" \
        "cat ${VISIBLEPW_FP}" \
      ; do
        echo -e "${MESSAGE_PREFIX} ${CMD}"
        eval "${CMD}"
      done

  else
    echo -e "${ERROR_PREFIX} could not write tempfile='${VISIBLEPW_TMP_FP}' to real file='${VISIBLEPW_FP}', exiting ..." 1>&2 # stderr
    exit 9
  fi

fi # [[ -z "${VISIBLEPW_FP}" ]]
exit 0
