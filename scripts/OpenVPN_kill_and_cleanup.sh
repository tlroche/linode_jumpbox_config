### Kill OpenVPN process, delete its logs.

### constants

## logging

THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
# for manual testing
#THIS_FN='OpenVPN_kill_and_cleanup.sh'
MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## other

OPENVPN_PROCESS_NAME='openvpn' # ASSERT: same on client and server
OPENVPN_PRIVATE_PROPERTIES_FP="${THIS_DIR}/private.properties"

### code

## `source` private properties: several constants below will not exist prior to this

if   [[ -z "${OPENVPN_PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} OPENVPN_PRIVATE_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  exit 2
elif [[ ! -r "${OPENVPN_PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read private properties='${OPENVPN_PRIVATE_PROPERTIES_FP}', exiting ..." 1>&2
  exit 3
else
  source ${OPENVPN_PRIVATE_PROPERTIES_FP}
fi

## Kill process=openvpn.
## TODO? stop?
## TODO: handle multiple processes

OPENVPN_PROCESS_N=$(pgrep -l ${OPENVPN_PROCESS_NAME} | wc -l)
if (( ${OPENVPN_PROCESS_N} <= 0 )) ; then
  echo -e "${MESSAGE_PREFIX} process='${OPENVPN_PROCESS_NAME}' not found: perhaps already killed?"
else
  for CMD in \
    "sudo pkill -9 ${OPENVPN_PROCESS_NAME}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
  done
fi

## Delete logs.
## TODO? backup?

for LOG_FP in \
  "${OPENVPN_MAIN_LOG_FP}" \
  "${OPENVPN_STATUS_LOG_FP}" \
; do
  if [[ ! -e "${LOG_FP}" ]] ; then
    echo -e "${MESSAGE_PREFIX} logfile='${LOG_FP}' not found: perhaps already cleaned-up?"
  else
    for CMD in \
      "sudo rm ${LOG_FP}" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ -e "${LOG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to delete logfile='${LOG_FP}', exiting ..." 1>&2 # stderr
    exit 4
  fi
done
echo # newline

exit 0
