#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Kill OpenVPN client process if it's running.
### TODO? failfast if !sudo? No: we need to know PUBLIC_PROPERTIES_FP :-(
### TODO: failfast if sudo ...
### TODO: refactor with start_OpenVPN_client.sh

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='kill_OpenVPN_client.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### other needed constants must be set in *.properties

### ----------------------------------------------------------------------
### get properties
### ----------------------------------------------------------------------

### TODO (for this and get_*.sh)? `source` and `export` in driver?

## public properties

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

## private properties (for OPENVPN_MAIN_LOG_FP==path to main log file)

if   [[ -z "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PRIVATE_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 4
  fi
elif [[ ! -r "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PRIVATE_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 5
  fi
else
  source "${PRIVATE_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

## do it

while [[ "$(pgrep -l ${OPENVPN_PROCESS_NAME} | wc -l)" != '0' ]] ; do
  sudo pkill -9 ${OPENVPN_PROCESS_NAME}
done

## show it (and its log)
## TODO: test who owns log, only use `sudo` as needed

for CMD in \
  "date" \
  "pgrep -l openvpn | wc -l" \
  "sudo ls -al ${OPENVPN_MAIN_LOG_FP}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done

## teardown

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
