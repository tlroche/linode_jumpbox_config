THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
# for manual testing
#THIS_FN='OpenVPN_install_client.sh'
MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### `source` code presumed to be in same dir/folder.

source ${THIS_DIR}/private.properties
source ${THIS_DIR}/my_linode_bash_library.sh

### Install client packages. TODO: check that `aptitude` is installed

sudo aptitude update
# package='resolvconf' needed to update client DNS.
sudo aptitude -y install openvpn resolvconf

exit 0
