#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Get OpenVPN routes and parse some information from them for future use. Accordingly, this will probably be `source`d.
### Requires:
### * `export`ed JUMPBOX_PUBLIC_IPN (the public IP# of the jumpbox
###   https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home#rst-header-jumpbox
###   running the OpenVPN server)
### * bash new enough to support all the string-manipulation woo-hoo
### * basename, dirname
### * fgrep
### * `ip` tools including `ip route`
### * readlink
### * sed (trim/remove whitespace)

### Requires path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
### Presuming you checked out the project to "${HOME}/code/bash/linode/linode_jumpbox_config",
### you could pass the path like:
### > DIR="${HOME}/code/bash/linode/linode_jumpbox_config/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" source "${DIR}/get_OpenVPN_routes.sh"

### TODO:
### * ensure we have already started OpenVPN client! ideally, by starting it ourself

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='get_OpenVPN_routes.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### get properties
### ----------------------------------------------------------------------

### other needed constants must be set in *.properties
### TODO (for this and get_*.sh)? `source` and `export` in driver?

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

if   [[ -z "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PRIVATE_PROPERTIES_FP not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 4
  fi
elif [[ ! -r "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PRIVATE_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 5
  fi
else
  source "${PRIVATE_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

### ----------------------------------------------------------------------
### get route to OpenVPN public IP#
### ----------------------------------------------------------------------

if   [[ -z "${JUMPBOX_PUBLIC_IPN}" ]] ; then

  echo -e "${ERROR_PREFIX} OpenVPN server public IP# JUMPBOX_PUBLIC_IPN not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 6
  fi

# elif # TODO: test ${JUMPBOX_PUBLIC_IPN}
else

#  echo -e "${MESSAGE_PREFIX} OpenVPN server public IP#=='${JUMPBOX_PUBLIC_IPN}'" # testing
  OPENVPN_PUBLIC_IPN_ROUTE_RAW="$(ip route show | grep -e "^${JUMPBOX_PUBLIC_IPN} via")"
  if [[ -z "${OPENVPN_PUBLIC_IPN_ROUTE_RAW}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to get route to OpenVPN server public IP#, exiting ..." 1>&2 # stderr
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 7
    fi
  else
    # trim the route, JIC (both leading and trailing)
    OPENVPN_PUBLIC_IPN_ROUTE="$(echo -e "${OPENVPN_PUBLIC_IPN_ROUTE_RAW}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
    if [[ -z "${OPENVPN_PUBLIC_IPN_ROUTE}" ]] ; then
      echo -e "${ERROR_PREFIX} failed to *trim* route to OpenVPN server public IP#, exiting ..." 1>&2
      # not for `source`ing
      if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
	exit 8
      fi
    else # success
      echo -e "${MESSAGE_PREFIX} OpenVPN route to public IP#='${OPENVPN_PUBLIC_IPN_ROUTE}'"
    fi

  fi # [[ -z "${OPENVPN_PUBLIC_IPN_ROUTE_RAW}" ]]

fi # [[ -z "${JUMPBOX_PUBLIC_IPN}" ]]

### ----------------------------------------------------------------------
### get/parse OpenVPN "kernel route"
### ----------------------------------------------------------------------

# Note following is heavily dependent on `ip route` grammar
# TODO: test OPENVPN_DEVICE_NAME

## local var containing ... what kind of route is this, anyway?
OPENVPN_KERNEL_ROUTE_RAW="$(ip route show | fgrep -e "${OPENVPN_DEVICE_NAME}" | fgrep -e 'proto kernel')"
if [[ -z "${OPENVPN_KERNEL_ROUTE_RAW}" ]] ; then
  echo -e "${ERROR_PREFIX} failed to get route to OpenVPN server public IP#, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 9
  fi
else
  # trim the route, JIC (both leading and trailing)
  OPENVPN_KERNEL_ROUTE="$(echo -e "${OPENVPN_KERNEL_ROUTE_RAW}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
  if [[ -z "${OPENVPN_KERNEL_ROUTE}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to *trim* route to OpenVPN server public IP#, exiting ..." 1>&2
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 10
    fi
  else # success
    export OPENVPN_KERNEL_ROUTE
    echo -e "${MESSAGE_PREFIX} OpenVPN 'kernel route'='${OPENVPN_KERNEL_ROUTE}'"
  fi

fi # [[ -z "${OPENVPN_KERNEL_ROUTE_RAW}" ]]

## more local vars, since we gotta remove all spaces
OPENVPN_ENDPT_IPN_RAW="${OPENVPN_KERNEL_ROUTE%% dev ${OPENVPN_DEVICE_NAME}*}"
OPENVPN_SRCPT_IPN_RAW="${OPENVPN_KERNEL_ROUTE##*src }"

OPENVPN_ENDPT_IPN="$(echo -e ${OPENVPN_ENDPT_IPN_RAW} | sed -e 's/ //g')"
# echo -e "OPENVPN_ENDPT_IPN='${OPENVPN_ENDPT_IPN}'"
OPENVPN_SRCPT_IPN="$(echo -e ${OPENVPN_SRCPT_IPN_RAW} | sed -e 's/ //g')"
# echo -e "OPENVPN_SRCPT_IPN='${OPENVPN_SRCPT_IPN}'"

## this seems like an incredibly crude/brittle way to find/verify the gateway address
OPENVPN_ENDPT_GATEWAY="${OPENVPN_ENDPT_IPN%.*}.1"
# echo -e "OPENVPN_ENDPT_GATEWAY='${OPENVPN_ENDPT_GATEWAY}'"
OPENVPN_SRCPT_GATEWAY="${OPENVPN_SRCPT_IPN%.*}.1"
# echo -e "OPENVPN_SRCPT_GATEWAY='${OPENVPN_SRCPT_GATEWAY}'"
if [[ "${OPENVPN_ENDPT_GATEWAY}" != "${OPENVPN_SRCPT_GATEWAY}" ]] ; then

  echo -e "${ERROR_PREFIX} cannot read properties file='${PRIVATE_PROPERTIES_FP}', exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 11
  fi

else # success!

  OPENVPN_GATEWAY_IPN="${OPENVPN_ENDPT_GATEWAY}"
  echo -e "${MESSAGE_PREFIX} OpenVPN gateway IP#=    '${OPENVPN_GATEWAY_IPN}'"
  echo -e "${MESSAGE_PREFIX} OpenVPN endpoint IP#=   '${OPENVPN_ENDPT_IPN}'"
  echo -e "${MESSAGE_PREFIX} OpenVPN sourcepoint IP#='${OPENVPN_SRCPT_IPN}'"

fi

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
