source ~/my_linode_bash_library.sh

# check copy source
ls -ald /usr/share/doc/openvpn/examples/easy-rsa/
ls -al /usr/share/doc/openvpn/examples/easy-rsa/
# check copy target
ls -ald /etc/openvpn/easy-rsa/
ls -al /etc/openvpn/easy-rsa/
# remove previous work (if present)
sudo rm -fr /etc/openvpn/easy-rsa/

### Copy easy-rsa
sudo rsync --append --archive -h /usr/share/doc/openvpn/examples/easy-rsa/ /etc/openvpn/easy-rsa/
# check copy target
ls -ald /etc/openvpn/easy-rsa/
ls -al /etc/openvpn/easy-rsa/
# > drwxr-xr-x 2 root root 4096 Nov  6 14:30 1.0
# > drwxr-xr-x 3 root root 4096 Nov  6 14:30 2.0

### Create openssl.cnf symlink
pushd /etc/openvpn/easy-rsa/2.0/
# test file exists
ls -al ./openssl-1.0.0.cnf
sudo ln -s ./openssl-1.0.0.cnf ./openssl.cnf
ls -al ./openssl.cnf
# popd? no, we'll stay in easy-rsa/2.0/

### Verify no keys
sudo ls -al /etc/openvpn/easy-rsa/2.0/keys/
# should fail:
# > ls: cannot access /etc/openvpn/easy-rsa/2.0/keys/: No such file or directory

### [edit, source] easy-rsa script= vars

## backup

FP='/etc/openvpn/easy-rsa/2.0/vars'
sudo_backup ${FP}

## edit

FP='/etc/openvpn/easy-rsa/2.0/vars'
sudo sed -i -e 's|export KEY_CONFIG=`$EASY_RSA/whichopensslcnf $EASY_RSA`|export KEY_CONFIG="$EASY_RSA/openssl.cnf"|' ${FP}
sudo sed -i -e 's|echo NOTE: If you run ./clean-all, I will be doing a rm -rf on $KEY_DIR|echo NOTE: If you run ./clean-all, I will be doing a rm -rf on $KEY_DIR\nls -al ${KEY_DIR}|' ${FP}
sudo sed -i -e 's|export KEY_COUNTRY="US"|export KEY_COUNTRY="US"|' ${FP}
sudo sed -i -e 's|export KEY_PROVINCE="CA"|export KEY_PROVINCE="NC"|' ${FP}
sudo sed -i -e 's|export KEY_CITY="SanFrancisco"|export KEY_CITY="Carrboro"|' ${FP}
sudo sed -i -e 's|export KEY_ORG="Fort-Funston"|export KEY_ORG="AQMEII-NA-N2O"|' ${FP}
# not sure why they use 2 lines for email
sudo sed -i -e 's|export KEY_EMAIL="me@myhost.mydomain"|export KEY_EMAIL="Tom_Roche@pobox.com"|' ${FP}
# not sure why this doesn't work
#sed -i -e 's|export KEY_EMAIL=mail@host.domain\n||' ${FP}
sudo sed -i -e 's|^export KEY_EMAIL=mail@host.domain||' ${FP} # leaves a blank line--so sue me
sudo sed -i -e 's|export KEY_CN=changeme|export KEY_CN="TomRoche"|' ${FP}
sudo sed -i -e 's|export KEY_NAME=changeme|export KEY_NAME="Tom Roche"|' ${FP}
#sed -i -e 's|export KEY_OU=changeme|export KEY_OU="TomRoche"\n# "only relevant for hardware cryptographic tokens"|' ${FP}
# should be 2 lines:
sudo sed -i -e 's|export KEY_OU=changeme|export KEY_OU="TomRoche"|' ${FP}
sudo sed -i -e 's|export PKCS11_MODULE_PATH=changeme|# "only relevant for hardware cryptographic tokens"\nexport PKCS11_MODULE_PATH=changeme|' ${FP}
# test
diff -uwB ${FP}.0 ${FP}

### Copy files to be re-`source`ed, since next section needs to run as root (i.e., not sudo):

for FN in \
  'private.properties' \
  'StackScript_Bash_Library.sh' \
  'my_linode_bash_library.sh' \
; do
  sudo cp ~/${FN} /root/
done
