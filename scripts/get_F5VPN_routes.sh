#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Get F5VPN routes and parse some information from them for future use. Accordingly, this will probably be `source`d.
### Unfortunately this script cannot expect to have F5VPN_PUBLIC_IPN (the public IP# of the F5VPN server) `export`ed to it
### (the way get_OpenVPN_routes.sh has JUMPBOX_PUBLIC_IPN passed to it) due to the networking problem that we're trying to fix.

### Requires:
### * `export`ed OPENVPN_ENDPT_IPN (IP# of OpenVPN endpoint), which marks route containing
###   F5VPN_PUBLIC_IPN (the public IP# of the F5VPN server to which we're connected)
### * bash new enough to support all the string-manipulation woo-hoo
### * basename, dirname
### * fgrep
### * `ip` tools including `ip route`
### * readlink
### * sed (trim/remove whitespace)

### Requires path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
### Presuming you checked out the project to "${HOME}/code/bash/linode/linode_jumpbox_config",
### you could pass the path like:
### > DIR="${HOME}/code/bash/linode/linode_jumpbox_config/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" source "${DIR}/get_F5VPN_routes.sh"

### TODO:
### * 

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='get_F5VPN_routes.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## some network constants. TODO? move to public.properties?
LEFTMOST_BIT_0='0.0.0.0/1'   # CIDR for "match all addresses with the leftmost bit cleared"
LEFTMOST_BIT_1='128.0.0.0/1' # CIDR for "match all addresses with the leftmost bit set"

### ----------------------------------------------------------------------
### get properties
### ----------------------------------------------------------------------

### other needed constants must be set in *.properties
### TODO (for this and get_*.sh)? `source` and `export` in driver?

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

if   [[ -z "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PRIVATE_PROPERTIES_FP not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 4
  fi
elif [[ ! -r "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PRIVATE_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 5
  fi
else
  source "${PRIVATE_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

### ----------------------------------------------------------------------
### get route to F5VPN server's public IP# via OpenVPN endpoint
### ----------------------------------------------------------------------

if   [[ -z "${OPENVPN_ENDPT_IPN}" ]] ; then
  echo -e "${ERROR_PREFIX} OPENVPN_ENDPT_IPN not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 6
  fi
else

  ## Use that to get the route containing F5VPN_PUBLIC_IPN
  F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE_RAW="$(ip route show | fgrep -e "via ${OPENVPN_ENDPT_IPN}")"

  if [[ -z "${F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE_RAW}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to get route to F5VPN server public IP#, exiting ..." 1>&2 # stderr
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 7
    fi
  else
    # trim the route, JIC (both leading and trailing)
    F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE="$(echo -e "${F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE_RAW}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
    if [[ -z "${F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE}" ]] ; then
      echo -e "${ERROR_PREFIX} failed to *trim* route to F5VPN server public IP#, exiting ..." 1>&2
      # not for `source`ing
      if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
        exit 8
      fi
    else # success
      echo -e "${MESSAGE_PREFIX} F5VPN route to public IP#='${F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE}'"
      F5VPN_PUBLIC_IPN="${F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE%% via*}"
      if [[ -z "${F5VPN_PUBLIC_IPN}" ]] ; then
        echo -e "${ERROR_PREFIX} failed to get F5VPN server public IP# from route to it??? exiting ..." 1>&2
        # not for `source`ing
        if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
          exit 9
        fi
      else # success!
        echo -e "${MESSAGE_PREFIX} F5VPN server public IP#='${F5VPN_PUBLIC_IPN}'"
      fi # [[ -z "${F5VPN_PUBLIC_IPN}" ]]
    fi # [[ -z "${F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE}" ]]

  fi # [[ -z "${F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE_RAW}" ]]
fi # [[ -z "${OPENVPN_ENDPT_IPN}" ]]

### ----------------------------------------------------------------------
### get F5VPN endpoint and "lowbit" route
### ----------------------------------------------------------------------

### TODO: trim first to ensure matching '^'
F5VPN_LOWBIT_ROUTE_RAW="$(ip route show | grep -e "^${LEFTMOST_BIT_0} via ")"

if [[ -z "${F5VPN_LOWBIT_ROUTE_RAW}" ]] ; then
  echo -e "${ERROR_PREFIX} failed to get F5VPN route for '${LEFTMOST_BIT_0}', exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 10
  fi
else
  # trim the route, JIC (both leading and trailing)
  F5VPN_LOWBIT_ROUTE="$(echo -e "${F5VPN_LOWBIT_ROUTE_RAW}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
  if [[ -z "${F5VPN_LOWBIT_ROUTE}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to *trim* F5VPN route for '${LEFTMOST_BIT_0}', exiting ..." 1>&2
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 11
    fi
  else # success
    echo -e "${MESSAGE_PREFIX} F5VPN route for '${LEFTMOST_BIT_0}'='${F5VPN_LOWBIT_ROUTE}'"
    ## and get F5VPN_ENDPT_IPN from the route
    F5VPN_ENDPT_IPN_RAW1="${F5VPN_LOWBIT_ROUTE%% dev *}"
    F5VPN_ENDPT_IPN_RAW2="${F5VPN_ENDPT_IPN_RAW1##*${LEFTMOST_BIT_0} via }"
    # and trim to be sure
    F5VPN_ENDPT_IPN="$(echo -e "${F5VPN_ENDPT_IPN_RAW2}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
    if [[ -z "${F5VPN_ENDPT_IPN}" ]] ; then
      echo -e "${ERROR_PREFIX} failed to get F5VPN endpoint IP# from route via it??? exiting ..." 1>&2
      # not for `source`ing
      if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
        exit 12
      fi
    else # success!
      echo -e "${MESSAGE_PREFIX} F5VPN endpoint IP#='${F5VPN_ENDPT_IPN}'"
    fi # [[ -z "${F5VPN_ENDPT_IPN}" ]]
  fi # [[ -z "${F5VPN_LOWBIT_ROUTE}" ]]
fi # [[ -z "${F5VPN_LOWBIT_ROUTE_RAW}" ]]

### ----------------------------------------------------------------------
### get "highbit" route using F5VPN endpoint
### ----------------------------------------------------------------------

### TODO: trim first to ensure matching '^'
F5VPN_HIGHBIT_ROUTE_RAW="$(ip route show | grep -e "^${LEFTMOST_BIT_1} via ${F5VPN_ENDPT_IPN}")"

if [[ -z "${F5VPN_HIGHBIT_ROUTE_RAW}" ]] ; then
  echo -e "${ERROR_PREFIX} failed to get F5VPN route for '${LEFTMOST_BIT_1}', exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 13
  fi
else
  # trim the route, JIC (both leading and trailing)
  F5VPN_HIGHBIT_ROUTE="$(echo -e "${F5VPN_HIGHBIT_ROUTE_RAW}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
  if [[ -z "${F5VPN_HIGHBIT_ROUTE}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to *trim* F5VPN route for '${LEFTMOST_BIT_1}', exiting ..." 1>&2
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 14
    fi
  else # success
    echo -e "${MESSAGE_PREFIX} F5VPN route for '${LEFTMOST_BIT_1}'='${F5VPN_HIGHBIT_ROUTE}'"
  fi # [[ -z "${F5VPN_HIGHBIT_ROUTE}" ]]
fi # [[ -z "${F5VPN_HIGHBIT_ROUTE_RAW}" ]]

### ----------------------------------------------------------------------
### get F5VPN gateway route using F5VPN endpoint
### ----------------------------------------------------------------------

### TODO: trim first to ensure matching '$'
# F5VPN_GATEWAY_VIA_ENDPT_ROUTE_RAW="$(ip route show | grep -e "src ${F5VPN_ENDPT_IPN}$")"
# not sure why the above fails, try
# F5VPN_GATEWAY_VIA_ENDPT_ROUTE_RAW="$(ip route show | grep -e "src[[:space:]]*${F5VPN_ENDPT_IPN}$")"
# nope, it's because it's untrimmed -> omit trailing '$'
F5VPN_GATEWAY_VIA_ENDPT_ROUTE_RAW="$(ip route show | grep -e "src[[:space:]]*${F5VPN_ENDPT_IPN}")"

if [[ -z "${F5VPN_GATEWAY_VIA_ENDPT_ROUTE_RAW}" ]] ; then
  echo -e "${ERROR_PREFIX} failed to get route to F5VPN gateway, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 15
  fi
else
  # trim the route, JIC (both leading and trailing)
  F5VPN_GATEWAY_VIA_ENDPT_ROUTE="$(echo -e "${F5VPN_GATEWAY_VIA_ENDPT_ROUTE_RAW}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
  if [[ -z "${F5VPN_GATEWAY_VIA_ENDPT_ROUTE}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to *trim* route to F5VPN gateway, exiting ..." 1>&2
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 16
    fi
  else # success
    echo -e "${MESSAGE_PREFIX} route to F5VPN gateway='${F5VPN_GATEWAY_VIA_ENDPT_ROUTE}'"
    F5VPN_GATEWAY_IPN_RAW="${F5VPN_GATEWAY_VIA_ENDPT_ROUTE%% dev *}"
    # and remove all whitespace, to be sure
    F5VPN_GATEWAY_IPN="$(echo -e "${F5VPN_GATEWAY_IPN_RAW}" | tr -d '[[:space:]]')"
    if [[ -z "${F5VPN_GATEWAY_IPN}" ]] ; then
      echo -e "${ERROR_PREFIX} failed to get F5VPN gateway IP# from route to it??? exiting ..." 1>&2
      # not for `source`ing
      if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
        exit 17
      fi
    else # success!
      echo -e "${MESSAGE_PREFIX} F5VPN gateway IP#='${F5VPN_GATEWAY_IPN}'"
    fi # [[ -z "${F5VPN_GATEWAY_IPN}" ]]
  fi # [[ -z "${F5VPN_GATEWAY_VIA_ENDPT_ROUTE}" ]]
fi # [[ -z "${F5VPN_GATEWAY_VIA_ENDPT_ROUTE_RAW}" ]]

### ----------------------------------------------------------------------
### create route to F5VPN gateway via OpenVPN gateway
### ----------------------------------------------------------------------

if   [[ -z "${F5VPN_GATEWAY_IPN}" ]] ; then
  echo -e "${ERROR_PREFIX} F5VPN_GATEWAY_IPN undefined? exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 18
  fi
elif [[ -z "${OPENVPN_GATEWAY_IPN}" ]] ; then
  echo -e "${ERROR_PREFIX} OPENVPN_GATEWAY_IPN undefined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 19
  fi
elif [[ -z "${OPENVPN_DEVICE_NAME}" ]] ; then
  echo -e "${ERROR_PREFIX} OPENVPN_DEVICE_NAME undefined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 19
  fi
else # make the route
  F5VPN_GATEWAY_VIA_OPENVPN_GATEWAY_ROUTE="${F5VPN_GATEWAY_IPN} via ${OPENVPN_GATEWAY_IPN} dev ${OPENVPN_DEVICE_NAME}"
fi # [[ -z "${F5VPN_GATEWAY_IPN}" ]]

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
