#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Restore "original local links" present on the client (not the linode, but the box from which you are `ssh`ing to the linode)
### Note that:
### * `link` == interface, as in `ip link`
### * this doesn't create any links, just deletes them
### * best to run with `sudo`, e.g., `sudo ${PATH_TO_PROJECT}/scripts/restore_original_links.sh`

### Requires:
### * LOCAL_F5VPN_LINK , LOCAL_OPENVPN_LINK from public.properties (`source`d below)
### * basename, dirname, readlink (but only for logging)
### * `ip` tools including `ip route`
### *

### Requires path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
### Presuming you checked out the project to "${HOME}/code/bash/linode/linode_jumpbox_config",
### you could pass the path like:
### > DIR="${HOME}/code/bash/linode/linode_jumpbox_config/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" source "${DIR}/get_local_routes.sh"

### TODO:
### * ensure we have no running clients={OpenVPN, F5VPN}

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}=='-bash'

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='restore_original_links.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### other needed constants must be set in *.properties

### ----------------------------------------------------------------------
### get properties
### ----------------------------------------------------------------------

### TODO (for this and get_*.sh)? `source` and `export` in driver?

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

if   [[ -z "${LOCAL_OPENVPN_LINK}" ]] ; then
  echo -e "${ERROR_PREFIX} LOCAL_OPENVPN_LINK not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 4
  fi
elif [[ -z "${LOCAL_F5VPN_LINK}" ]] ; then
  echo -e "${ERROR_PREFIX} LOCAL_F5VPN_LINK not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 5
  fi
else

  for LINK in "${LOCAL_OPENVPN_LINK}" "${LOCAL_F5VPN_LINK}" ; do
    ## TODO: pretest with `ip link show ${LINK}`
    for CMD in \
      "sudo ip link del dev ${LINK}" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}"
      eval "${CMD}"
      ## note "error" == "Cannot find device <whatever/>" is OK
    done
    echo # newline
  done

fi

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

for CMD in \
  "date" \
  "ip link show" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
  echo # newline
done

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
