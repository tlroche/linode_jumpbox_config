#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Do some crude network testing, presumably from the client device https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home#rst-header-client-device

### Requires:
### * path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
###   Presuming you checked out the project to "${HOME}/code/bash/linode/linode_jumpbox_config",
###   you could pass the path like:
###   > DIR="${HOME}/code/bash/linode/linode_jumpbox_config/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" source "${DIR}/get_public_IP_n.sh"
### * basename, dirname, readlink (just for logging)
### * ping
### * nslookup
### * source (TODO: set outside of script)

### TODO: check error/return codes from test commands!

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='network_test.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

### ----------------------------------------------------------------------
### get properties
### ----------------------------------------------------------------------

## other needed properties must be set here, which must be set by caller
if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### payload
### ----------------------------------------------------------------------

### ----------------------------------------------------------------------
### test raw `ping`
### ----------------------------------------------------------------------

if   [[ -z "${IP_GETTER_IPN}" ]] ; then
  echo -e "${ERROR_PREFIX} could not retrieve IP_GETTER_IPN, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 4
  fi
elif [[ -z "${PING_CMD}" ]] ; then
  echo -e "${ERROR_PREFIX} could not retrieve PING_CMD, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 5
  fi
else

  for CMD in "${PING_CMD}" ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
    ## TODO: test error/return code
  done
  echo # newline
fi

### ----------------------------------------------------------------------
### test DNS
### ----------------------------------------------------------------------

if   [[ -z "${IP_GETTER_FQDN}" ]] ; then
  echo -e "${ERROR_PREFIX} could not retrieve IP_GETTER_FQDN, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 6
  fi
elif [[ -z "${DNS_CMD}" ]] ; then
  echo -e "${ERROR_PREFIX} could not retrieve DNS_CMD, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 7
  fi
else

  for CMD in "${DNS_CMD}" ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
    ## TODO: test error/return code
  done
  echo # newline
fi
