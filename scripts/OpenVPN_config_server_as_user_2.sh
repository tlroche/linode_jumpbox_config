source ~/my_linode_bash_library.sh
source ~/private.properties

### Configure ``server.conf``

## original contents/instructions derived from https://www.linode.com/docs/networking/vpn/secure-communications-with-openvpn-on-ubuntu-12-04-precise-and-debian-7
# looks like we just plain *use* it:
SOURCE_FP='/usr/share/doc/openvpn/examples/sample-config-files/server.conf'
SOURCE_DIR="$(dirname ${SOURCE_FP})"
# TARGET_DIR="${OPENVPN_CLIENT_TARGET_DIR}"
# nope, these are also for client, and we haven't parameterized for server
TARGET_DIR='/etc/openvpn'
FN="$(basename ${SOURCE_FP})"
TARGET_FP="${TARGET_DIR}/${FN}"
BACKEDUP_FP="${SOURCE_FP}.0"
COMPRESSED_FP="${SOURCE_FP}.gz"
COMPRESSED_FN="$(basename ${COMPRESSED_FP})"

## these contents copy/mod from https://wiki.debian.org/openvpn%20for%20server%20and%20client
## note need for internal quote-escaping!
sudo sh -c "echo '# [server.conf]
port ${OPENVPN_PORT_NUMBER}
proto udp
dev tun
ca ${TARGET_DIR}/ca.crt
cert ${TARGET_DIR}/server.crt
key ${TARGET_DIR}/server.key
dh ${TARGET_DIR}/dh1024.pem
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
push \"dhcp-option DNS ${OPENVPN_DNS_PRIMARY}\"
push \"dhcp-option DNS ${OPENVPN_DNS_SECONDARY}\"
# next line required by https://www.linode.com/docs/networking/vpn/secure-communications-with-openvpn-on-ubuntu-12-04-precise-and-debian-7#tunneling-all-connections-through-the-vpn to fix ultra-VPN routing
# (i.e., not provided by https://wiki.debian.org/openvpn%20for%20server%20and%20client )
push \"dhcp-option DNS 10.8.0.1\"
# next line also required by https://www.linode.com/docs/networking/vpn/secure-communications-with-openvpn-on-ubuntu-12-04-precise-and-debian-7#tunneling-all-connections-through-the-vpn to fix ultra-VPN routing
# (i.e., also provided by https://wiki.debian.org/openvpn%20for%20server%20and%20client )
push \"redirect-gateway def1 bypass-dhcp\"
keepalive 10 120
comp-lzo
user nobody
group nogroup
persist-key
persist-tun
# logging
log-append ${OPENVPN_MAIN_LOG_FP}
status ${OPENVPN_STATUS_LOG_FP}
verb 3' > ${TARGET_FP}" # double-quote ends `sh -c`, single-quote ends `echo`
ls -al ${TARGET_FP}
cat ${TARGET_FP}

### Configure server ``/etc/default/openvpn`` to *not* autostart. **TODO:** autostart all, or autostart client1?

FP='/etc/default/openvpn'
VAR='AUTOSTART="none"'
fgrep -nH -e ${VAR} ${FP}
sudo_backup ${FP}

# TODO: don't rely on this particular style of commenting!
sudo sed -i -e "s/^#${VAR}/${VAR}/" ${FP}
fgrep -nH -e ${VAR} ${FP}

### "Configure" ``client.conf`` on server. The server won't actually *use* this copy of ``client.conf``, I'm just making it easier to retrieve from the actual client.

## original contents/instructions derived from https://www.linode.com/docs/networking/vpn/secure-communications-with-openvpn-on-ubuntu-12-04-precise-and-debian-7
SOURCE_FP='/usr/share/doc/openvpn/examples/sample-config-files/client.conf'
SOURCE_DIR="$(dirname ${SOURCE_FP})"
TARGET_FP="${OPENVPN_CLIENT_USER_HOME}/${OPENVPN_CLIENT_CONF_FN}"
BACKEDUP_FP="${SOURCE_FP}.0"
TARGET_DIR="${OPENVPN_CLIENT_TARGET_DIR}" # this *will* be for client

## these contents nearly copied from https://wiki.debian.org/openvpn%20for%20server%20and%20client (substituting OPENVPN_CLIENT_NAME etc)
# sudo sh -c "echo '# [client.conf] # NO! want this NOT root-owned
echo -e "# [client.conf]
client
dev tun
proto udp
remote ${MYNODE_IPV4} ${OPENVPN_PORT_NUMBER}
resolv-retry infinite
nobind
user nobody
group nogroup
persist-key
persist-tun
mute-replay-warnings
ca ${TARGET_DIR}/ca.crt
cert ${TARGET_DIR}/${OPENVPN_CLIENT_NAME}.crt
key ${TARGET_DIR}/${OPENVPN_CLIENT_NAME}.key
ns-cert-type server
comp-lzo
# logging
log-append ${OPENVPN_MAIN_LOG_FP}
status ${OPENVPN_STATUS_LOG_FP}
verb 3 # not at end
script-security 2 # per Chris Davies https://groups.google.com/d/msg/comp.os.linux.networking/xkpZLHxcf24/rmY-GUngTFMJ
up ${TARGET_DIR}/update-resolv-conf
down ${TARGET_DIR}/update-resolv-conf" > ${TARGET_FP}
ls -al ${TARGET_FP}
cat ${TARGET_FP}
