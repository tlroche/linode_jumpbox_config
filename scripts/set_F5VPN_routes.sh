#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Set routes for F5VPN when tunneling through OpenVPN, after it sets its own routes (that won't work with OpenVPN).
### Requires:
### * lots of previously-determined routes set as variables in the current environment, probably by `get_F5VPN_routes.sh`
### * (probably) F5VPN-set routes deleted (e.g., by `clear_F5VPN_routes.sh`).
###   I say 'probably' because some of this script's `route add`s produce 'RTNETLINK answers: File exists' otherwise.
### * `ip route`
### * `sudo` status, to `sudo ip route add`
### * basename, dirname, readlink (but only for logging)
### * `bash` new enough to do arrays
### * paths set in parent environment:
### ** path to private.properties=PRIVATE_PROPERTIES_FP
### ** path to public.properties=PUBLIC_PROPERTIES_FP

### TODO:
### * 

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}

if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
else # use SCRIPTS_DIR if available to find other dependencies
  THIS_FN='set_F5VPN_routes.sh'
  if [[ -n "${SCRIPTS_DIR}" && -d "${SCRIPTS_DIR}" ]] ; then
    THIS_DIR="${SCRIPTS_DIR}"
  else
    THIS_DIR='.' # will probably end badly
  fi
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### get properties
### ----------------------------------------------------------------------

### other needed constants must be set in *.properties
### TODO (for this and get_*.sh)? `source` and `export` in driver?

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

if   [[ -z "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PRIVATE_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 4
  fi
elif [[ ! -r "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PRIVATE_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 5
  fi
else
  source "${PRIVATE_PROPERTIES_FP}"
fi

if   [[ -z "${MY_IP_ROUTE_LIBRARY_FN}" ]] ; then # from public.properties
  echo -e "${ERROR_PREFIX} library path=MY_IP_ROUTE_LIBRARY_FN is not defined, exiting ..." 1>&2 # stderr
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 6
  fi
else
  MY_IP_ROUTE_LIBRARY_FP="${THIS_DIR}/${MY_IP_ROUTE_LIBRARY_FN}"
  if [[ ! -r "${MY_IP_ROUTE_LIBRARY_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read library='${MY_IP_ROUTE_LIBRARY_FP}', exiting ..." 1>&2
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 7
    fi
  else
    source "${MY_IP_ROUTE_LIBRARY_FP}" # TODO: test result
  fi
fi

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

### ----------------------------------------------------------------------
### get routes
### ----------------------------------------------------------------------

# echo -e "${MESSAGE_PREFIX} length(F5VPN_ROUTES)=='${#F5VPN_ROUTES[*]}'"
# > set_F5VPN_routes.sh: length(F5VPN_ROUTES)=='0'
# TODO: clear F5VPN_ROUTES if !empty

## LOCAL_IPN is client/laptop's LAN IP# (not public)
## setting LOCAL_IPN here only for testing
# LOCAL_IPN='192.168.1.142'
## LOCAL_IPN_ROUTE is part of "original routeset" on client/laptop
## setting LOCAL_IPN_ROUTE here only for testing
# LOCAL_IPN_ROUTE="192.168.1.0/24 dev eth0  proto kernel  scope link  src ${LOCAL_IPN}"
# F5VPN_ROUTES_I=0 # iterator for F5VPN_ROUTES == length(F5VPN_ROUTES) - 1

if   [[ -z "${LOCAL_IPN_ROUTE}" ]] ; then
  echo -e "${ERROR_PREFIX} LOCAL_IPN_ROUTE not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 8
  fi
else
#  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="${LOCAL_IPN_ROUTE}"
#  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="${LOCAL_IPN_ROUTE}"
  # no, `ip route add` will choke on routes containing 'proto none', so instead
  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="$(route_minus_proto_none "${LOCAL_IPN_ROUTE}")"
fi # [[ -z "${LOCAL_IPN_ROUTE}" ]]

# echo -e "${MESSAGE_PREFIX} length(F5VPN_ROUTES)=='${#F5VPN_ROUTES[*]}'"

if   [[ -z "${F5VPN_LOWBIT_ROUTE}" ]] ; then
  echo -e "${ERROR_PREFIX} F5VPN_LOWBIT_ROUTE not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 7
  fi
else
#  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="${F5VPN_LOWBIT_ROUTE}"
  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="$(route_minus_proto_none "${F5VPN_LOWBIT_ROUTE}")"
fi # [[ -z "${F5VPN_LOWBIT_ROUTE}" ]]

if   [[ -z "${LOCAL_DEFAULT_ROUTE}" ]] ; then
  echo -e "${ERROR_PREFIX} LOCAL_DEFAULT_ROUTE not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 8
  fi
else
#  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="${LOCAL_DEFAULT_ROUTE}"
  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="$(route_minus_proto_none "${LOCAL_DEFAULT_ROUTE}")"
fi # [[ -z "${LOCAL_DEFAULT_ROUTE}" ]]

if   [[ -z "${F5VPN_GATEWAY_VIA_ENDPT_ROUTE}" ]] ; then
  echo -e "${ERROR_PREFIX} F5VPN_GATEWAY_VIA_ENDPT_ROUTE not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 9
  fi
else
#  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="${F5VPN_GATEWAY_VIA_ENDPT_ROUTE}"
  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="$(route_minus_proto_none "${F5VPN_GATEWAY_VIA_ENDPT_ROUTE}")"
fi # [[ -z "${F5VPN_GATEWAY_VIA_ENDPT_ROUTE}" ]]

if   [[ -z "${F5VPN_HIGHBIT_ROUTE}" ]] ; then
  echo -e "${ERROR_PREFIX} F5VPN_HIGHBIT_ROUTE not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 10
  fi
else
#  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="${F5VPN_HIGHBIT_ROUTE}"
  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="$(route_minus_proto_none "${F5VPN_HIGHBIT_ROUTE}")"
fi # [[ -z "${F5VPN_HIGHBIT_ROUTE}" ]]

if   [[ -z "${OPENVPN_PUBLIC_IPN_ROUTE}" ]] ; then
  echo -e "${ERROR_PREFIX} OPENVPN_PUBLIC_IPN_ROUTE not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 11
  fi
else
#  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="${OPENVPN_PUBLIC_IPN_ROUTE}"
  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="$(route_minus_proto_none "${OPENVPN_PUBLIC_IPN_ROUTE}")"
fi # [[ -z "${OPENVPN_PUBLIC_IPN_ROUTE}" ]]

if   [[ -z "${F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE}" ]] ; then
  echo -e "${ERROR_PREFIX} F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 12
  fi
else
#  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="${F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE}"
  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="$(route_minus_proto_none "${F5VPN_PUBLIC_IPN_VIA_OPENVPN_ENDPT_ROUTE}")"
fi

if   [[ -z "${F5VPN_GATEWAY_VIA_OPENVPN_GATEWAY_ROUTE}" ]] ; then
  echo -e "${ERROR_PREFIX} F5VPN_GATEWAY_VIA_OPENVPN_GATEWAY_ROUTE not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 12
  fi
else
#  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="${F5VPN_GATEWAY_VIA_OPENVPN_GATEWAY_ROUTE}"
  F5VPN_ROUTES[${#F5VPN_ROUTES[*]}]="$(route_minus_proto_none "${F5VPN_GATEWAY_VIA_OPENVPN_GATEWAY_ROUTE}")"
fi

### ----------------------------------------------------------------------
### set routes
### ----------------------------------------------------------------------

## Not sure why this is the quoting to use, but I've used it before ...
let "F5VPN_ROUTES_N=${#F5VPN_ROUTES[*]}"
if (( F5VPN_ROUTES_N <= 0 )) ; then
  echo -e "${ERROR_PREFIX} |F5VPN routes| F5VPN_ROUTES_N='${F5VPN_ROUTES_N}', exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 13
  fi
else

  # set routes in queue order or stack order?
  for (( F5VPN_ROUTES_I=0 ; F5VPN_ROUTES_I<F5VPN_ROUTES_N ; F5VPN_ROUTES_I++ )) ; do
    F5VPN_ROUTE="${F5VPN_ROUTES[${F5VPN_ROUTES_I}]}" # local
    echo -e "${MESSAGE_PREFIX} about to set F5VPN route#=${F5VPN_ROUTES_I}: '${F5VPN_ROUTE}'"
    for CMD in \
      "sudo ip route add ${F5VPN_ROUTE}" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}"
      eval "${CMD}"
    done
    echo # newline
  done

fi # (( F5VPN_ROUTES_N <= 0 ))

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

# TODO: `show_routes`
for CMD in \
  "date" \
  "ip route show" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
