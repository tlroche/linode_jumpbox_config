#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Restore "original local routes" present on the client (not the linode, but the box from which you are `ssh`ing to the linode) *before* running OpenVPN or F5VPN clients.
### Best to run with `sudo`, e.g., `sudo ${PATH_TO_PROJECT}/scripts/restore_original_routes.sh`

### Requires:
### * LOCAL_IPN from ./get_local_routes.sh
### * LOCAL_NETWORK_LINK from public.properties (`source`d below)
### * bash new enough to support arrays and string-manipulation woo-hoo
### * basename, dirname
### * `ip` tools including `ip route`
### * readlink

### Requires path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
### Presuming you checked out the project to "${HOME}/code/bash/linode/linode_jumpbox_config",
### you could pass the path like:
### > DIR="${HOME}/code/bash/linode/linode_jumpbox_config/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" source "${DIR}/get_local_routes.sh"

### TODO:
### * ensure we have no running clients={OpenVPN, F5VPN}

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}=='-bash'

if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
else # use SCRIPTS_DIR if available to find other dependencies
  THIS_FN='restore_original_routes.sh'
  if [[ -n "${SCRIPTS_DIR}" && -d "${SCRIPTS_DIR}" ]] ; then
    THIS_DIR="${SCRIPTS_DIR}"
  else
    THIS_DIR='.' # will probably end badly
  fi
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

### other needed constants must be set in *.properties
### TODO (for this and get_*.sh)? `source` and `export` in driver?

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### dependencies
### ----------------------------------------------------------------------

if   [[ -z "${MY_IP_ROUTE_LIBRARY_FN}" ]] ; then # from public.properties
  echo -e "${ERROR_PREFIX} library path=MY_IP_ROUTE_LIBRARY_FN is not defined, exiting ..." 1>&2 # stderr
  exit 4
else
  MY_IP_ROUTE_LIBRARY_FP="${THIS_DIR}/${MY_IP_ROUTE_LIBRARY_FN}"
  if [[ ! -r "${MY_IP_ROUTE_LIBRARY_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read library='${MY_IP_ROUTE_LIBRARY_FP}', exiting ..." 1>&2
    exit 5
  else
    source "${MY_IP_ROUTE_LIBRARY_FP}" # TODO: test result
  fi
fi

### ----------------------------------------------------------------------
### payload
### ----------------------------------------------------------------------

### ----------------------------------------------------------------------
### setup routes to restore
### ----------------------------------------------------------------------

### TODO: gotta pass local IP#=LOCAL_IPN and local interface=LOCAL_NETWORK_LINK! else move ORIGINAL_ROUTES to *.properties

if   [[ -z "${LOCAL_IPN}" ]] ; then
  echo -e "${ERROR_PREFIX} client's LAN/local IP#=LOCAL_IPN not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 6
  fi
elif [[ -z "${LOCAL_NETWORK_LINK}" ]] ; then
  echo -e "${ERROR_PREFIX} client's LOCAL_NETWORK_LINK not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 7
  fi
else

  ORIGINAL_ROUTES[0]="default via 192.168.1.1 dev ${LOCAL_NETWORK_LINK}  proto static"
  ORIGINAL_ROUTES[1]="169.254.0.0/16 dev ${LOCAL_NETWORK_LINK}  scope link  metric 1000"
  ORIGINAL_ROUTES[2]="192.168.1.0/24 dev ${LOCAL_NETWORK_LINK}  proto kernel  scope link  src ${LOCAL_IPN}"
  ## TODO: test array
  #echo -e "length(ORIGINAL_ROUTES)=='${#ORIGINAL_ROUTES[*]}'"

  for (( OR_ITER=0 ; OR_ITER<${#ORIGINAL_ROUTES[*]} ; OR_ITER++ )) ; do
    echo -e "ORIGINAL_ROUTES[${OR_ITER}]=='${ORIGINAL_ROUTES[${OR_ITER}]}'"
  done
  echo # newline

fi

### ----------------------------------------------------------------------
### delete some routes
### ----------------------------------------------------------------------

show_routes

# Fri Feb 13 15:10:04 EST 2015
# default via 192.168.1.1 dev eth0  proto static 
# 169.254.0.0/16 dev eth0  scope link  metric 1000 
# 192.168.1.0/24 dev eth0  proto kernel  scope link  src 192.168.1.142 

# tested above
# if   [[ -z "${LOCAL_NETWORK_LINK}" ]] ; then
#   echo -e "${ERROR_PREFIX} client's LOCAL_NETWORK_LINK not defined, exiting ..." 1>&2
#   # not for `source`ing
#   if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
#     exit 8
#   fi
if   [[ -z "${LOCAL_OPENVPN_LINK}" ]] ; then
  echo -e "${ERROR_PREFIX} client's LOCAL_OPENVPN_LINK not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 9
  fi
elif [[ -z "${LOCAL_F5VPN_LINK}" ]] ; then
  echo -e "${ERROR_PREFIX} client's LOCAL_F5VPN_LINK not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 10
  fi
else

  for NI in "${LOCAL_F5VPN_LINK}" "${LOCAL_OPENVPN_LINK}" "${LOCAL_NETWORK_LINK}" ; do
    del_routes "${NI}"
  done

fi

show_routes

### ----------------------------------------------------------------------
### restore desired/original routes
### ----------------------------------------------------------------------

for (( OR_ITER=0 ; OR_ITER<${#ORIGINAL_ROUTES[*]} ; OR_ITER++ )) ; do

  for CMD in \
    "sudo ip route add ${ORIGINAL_ROUTES[${OR_ITER}]}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
    ## TODO: handle response='RTNETLINK answers: Network is unreachable':
    ## after this script runs, can just rerun `sudo ip route add <failed route/>`, works every time
  done
#  echo # newline

done
echo # newline

show_routes

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
