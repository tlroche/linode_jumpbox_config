THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
# for manual testing
#THIS_FN='OpenVPN_config_client_undo.sh'
MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### `source` code presumed to be in same dir/folder. TODO: test!g

source ${THIS_DIR}/private.properties
source ${THIS_DIR}/my_linode_bash_library.sh

### Delete client config and credentials previously copied from server.

for CLIENT_CONFIG_FN in \
  'ca.crt' \
  "${OPENVPN_CLIENT_NAME}.conf" \
  "${OPENVPN_CLIENT_NAME}.crt" \
  "${OPENVPN_CLIENT_NAME}.key" \
; do
  for DELETE_FP in \
    "${OPENVPN_CLIENT_STASH_DIR}/${CLIENT_CONFIG_FN}" \
    "${OPENVPN_CLIENT_TARGET_DIR}/${CLIENT_CONFIG_FN}" \
  ; do
#    if [[ -r "${DELETE_FP}" ]] ; then
    # Gotta test with '-e' since some of these files are owner=root permissions=600
    if [[ -e "${DELETE_FP}" ]] ; then
      echo -e "${WARNING_PREFIX} deleting client file='${DELETE_FP}'"
      sudo rm ${DELETE_FP}
    else
      echo -e "${ERROR_PREFIX} cannot delete missing client file='${DELETE_FP}'" 1>&2 # stderr
    fi
  done
  echo # newline
done
