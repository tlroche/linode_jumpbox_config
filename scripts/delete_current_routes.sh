#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Clear all current routes (presumably prior to setting new ones :-) Requires:
### * `ip route` and helpers (latter probably in ./ip_route_functions.sh )
### * basename, dirname, readlink (but only for logging)
### * wc
### * path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment

### TODO:
### * 

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

### just for logging

THIS="$0"
if [[ -z "${THIS}" || "${THIS}" == '-bash' ]] ; then
  echo -e "${ERROR_PREFIX} do not \`source\` this script! which will now proceed to fail ..." 1>&2
  # but not `exit` so as to not exit the current shell
fi

THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

### ----------------------------------------------------------------------
### get dependencies
### ----------------------------------------------------------------------

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
#  echo -e "${MESSAGE_PREFIX} about to \`source\` public properties from '${PUBLIC_PROPERTIES_FP}'"
  source "${PUBLIC_PROPERTIES_FP}"
fi

if   [[ -z "${MY_IP_ROUTE_LIBRARY_FN}" ]] ; then # from public.properties
  echo -e "${ERROR_PREFIX} library path=MY_IP_ROUTE_LIBRARY_FN is not defined, exiting ..." 1>&2 # stderr
  exit 4
else
  MY_IP_ROUTE_LIBRARY_FP="${THIS_DIR}/${MY_IP_ROUTE_LIBRARY_FN}"
  if [[ ! -r "${MY_IP_ROUTE_LIBRARY_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read library='${MY_IP_ROUTE_LIBRARY_FP}', exiting ..." 1>&2
    exit 5
  else
    source "${MY_IP_ROUTE_LIBRARY_FP}" # TODO: test result
  fi
fi

### ----------------------------------------------------------------------
### delete routes
### ----------------------------------------------------------------------

## testing
# echo -e "${MESSAGE_PREFIX} |current routes|==$(ip route show | wc -l):"
# show_routes
# echo # newline

## Don't need the actual routes if we let `del_routes` do this
# # for ROUTE_TO_DELETE in $(ip route show) ; do # No! won't handle spaces
# while read ROUTE_TO_DELETE ; do # instead, `read` output of command:
#   ROUTES_TO_DELETE+=("${ROUTE_TO_DELETE}")
# done < <(ip route show)
# #echo -e "${MESSAGE_PREFIX} |ROUTES_TO_DELETE|==${#ROUTES_TO_DELETE[*]}" # testing
# ## Not sure why this is the quoting to use, but I've used it before ...
# let "ROUTES_TO_DELETE_N=${#ROUTES_TO_DELETE[*]}"
let "ROUTES_TO_DELETE_N=$(ip route show | wc -l)"

if (( ROUTES_TO_DELETE_N <= 0 )) ; then
  echo -e "${ERROR_PREFIX} |F5VPN routes to delete|=='${ROUTES_TO_DELETE_N}', exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 6
  fi
else
  del_routes # TODO: test result
fi # (( ROUTES_TO_DELETE_N <= 0 ))

show_routes

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
