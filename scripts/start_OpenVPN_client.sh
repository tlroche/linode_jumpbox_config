#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### start OpenVPN client process if it's not already running.
### TODO? failfast if !sudo? No: we need to know PUBLIC_PROPERTIES_FP :-(
### TODO: failfast if sudo ...
### TODO: refactor with kill_OpenVPN_client.sh

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='start_OpenVPN_client.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### get properties
### ----------------------------------------------------------------------

### TODO (for this and get_*.sh)? `source` and `export` in driver?

## public properties

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

## private properties (for OPENVPN_MAIN_LOG_FP==path to main log file)

if   [[ -z "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PRIVATE_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 4
  fi
elif [[ ! -r "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PRIVATE_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 5
  fi
else
  source "${PRIVATE_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### get other constants
### ----------------------------------------------------------------------

## path to <client name/>.conf. TODO: property-ize

if   [[ -z "${OPENVPN_CLIENT_TARGET_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} OPENVPN_CLIENT_TARGET_DIR not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 6
  fi
elif [[ ! -d "${OPENVPN_CLIENT_TARGET_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read config file dir/folder='${OPENVPN_CLIENT_TARGET_DIR}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 7
  fi
elif [[ -z "${OPENVPN_CLIENT_CONF_FN}" ]] ; then
  echo -e "${ERROR_PREFIX} OPENVPN_CLIENT_CONF_FN not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 8
  fi
else

  OPENVPN_CLIENT_CONF_FP="${OPENVPN_CLIENT_TARGET_DIR}/${OPENVPN_CLIENT_CONF_FN}"
  if [[ ! -r "${OPENVPN_CLIENT_CONF_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read config file='${OPENVPN_CLIENT_CONF_FP}', exiting ..." 1>&2
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 9
    fi
  fi

fi # [[ -z "${OPENVPN_CLIENT_TARGET_DIR}" ]]

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

if [[ -z "${OPENVPN_PROCESS_NAME}" ]] ; then
  echo -e "${ERROR_PREFIX} OPENVPN_PROCESS_NAME not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 10
  fi
else

  ## start it (and show its log)
  ## TODO: test who owns log, only use `sudo` as needed

  if [[ "$(pgrep -l ${OPENVPN_PROCESS_NAME} | wc -l)" == '0' ]] ; then
    for CMD in \
      "date" \
      "sudo ${OPENVPN_PROCESS_NAME} --config ${OPENVPN_CLIENT_CONF_FP} &" \
      "sudo ls -al ${OPENVPN_MAIN_LOG_FP}" \
      "pgrep -l ${OPENVPN_PROCESS_NAME} | wc -l" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}"
      eval "${CMD}"
    done
  else
    echo -e "${WARNING_PREFIX} OpenVPN process='${OPENVPN_PROCESS_NAME}' already started, exiting ..." 1>&2
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 11
    fi
  fi

  ## check that it's up now
  if [[ "$(pgrep -l ${OPENVPN_PROCESS_NAME} | wc -l)" == '0' ]] ; then
    echo -e "${ERROR_PREFIX} OpenVPN process='${OPENVPN_PROCESS_NAME}' failed to start, exiting ..." 1>&2
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 12
    fi
  fi

fi # [[ -z "${OPENVPN_PROCESS_NAME}" ]]

## teardown

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
