#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Just some wrappers for `ip route` functionality, purely for `source`ing.

### Requires:
### * `ip route` from iproute2 https://en.wikipedia.org/wiki/Iproute2
### * bash new enough to support arrays and string-manipulation woo-hoo
### * basename, dirname, readlink (only for logging)

### Requires path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
### Presuming you checked out the project to "${HOME}/code/bash/linode/linode_jumpbox_config",
### you could pass the path like:
### > DIR="${HOME}/code/bash/linode/linode_jumpbox_config/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" source "${DIR}/get_local_routes.sh"

### TODO:
### * ensure we have no running clients={OpenVPN, F5VPN}

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}=='-bash'

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='ip_route_functions.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### functions
### ----------------------------------------------------------------------

function show_routes {
  local NETWORK_LINK="${1}"
  local CMD NI_CMD

  if [[ -z "${NETWORK_LINK}" ]] ; then
    NI_CMD='ip route show'
  else
    NI_CMD="ip route show dev ${NETWORK_LINK}"
  fi

  # TODO: say something if there are no routes to show (e.g., when all have been deleted)
  for CMD in \
    "date" \
    "${NI_CMD}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
  done

  echo # newline
} # function show_routes

### Remove 'proto none' from routes, since that annoys `ip route add`:
### > Error: argument "none" is wrong: "protocol" value is invalid
function route_minus_proto_none {
  local ROUTE="${1}"
  if [[ -z "${ROUTE}" ]] ; then
    echo -e "${ERROR_PREFIX} no argument, exiting ..." 1>&2
  else
    echo -e "$(echo -e "${ROUTE}" | sed -e 's/proto[[:space:]]*none//')"
  fi
} # function del_routes

function del_routes {
  local NETWORK_LINK="${1}"
  local ROUTE ROUTE_NO_PROTO_NONE

  if [[ -z "${NETWORK_LINK}" ]] ; then
    NI_CMD='ip route show'
  else
    NI_CMD="ip route show dev ${NETWORK_LINK}"
  fi

  while read ROUTE ; do
    ROUTE_NO_PROTO_NONE="$(route_minus_proto_none "${ROUTE}")"

    for CMD in \
      "sudo ip route del ${ROUTE_NO_PROTO_NONE}" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}"
      eval "${CMD}"
    done
  done < <(${NI_CMD})

} # function del_routes
