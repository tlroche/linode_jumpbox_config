#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Restore "original" DNS/resolvconf present on the client (not the linode, but the box from which you are `ssh`ing to the linode)
### Note that:
### * `link` == interface, as in `ip link`
### * this doesn't create any links, just deletes them
### * best to run with `sudo`, e.g., `sudo ${PATH_TO_PROJECT}/scripts/restore_original_links.sh`

### Requires:
### * DNS_TEST_HOSTNAME , LOCAL_RESOLVCONF_LINKNAME , LOCAL_RESOLVCONF_TARGET from public.properties (`source`d below)
### * basename, dirname, readlink (but only for logging)
### * ln
### * nslookup

### Requires path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
### Presuming you checked out the project to "${HOME}/code/bash/linode/linode_jumpbox_config",
### you could pass the path like:
### > DIR="${HOME}/code/bash/linode/linode_jumpbox_config/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" source "${DIR}/get_local_routes.sh"

### TODO:
### * set LOCAL_DNS_IPN (from public.properties) as DNS server
### * support multiple DNS servers (e.g., primary, secondary)

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}=='-bash'

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='restore_original_DNS.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### other needed constants must be set in *.properties

### ----------------------------------------------------------------------
### get properties
### ----------------------------------------------------------------------

### TODO (for this and get_*.sh)? `source` and `export` in driver?

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

if   [[ -z "${DNS_TEST_HOSTNAME}" ]] ; then
  echo -e "${ERROR_PREFIX} DNS_TEST_HOSTNAME not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 4
  fi
elif [[ -z "${LOCAL_RESOLVCONF_LINKNAME}" ]] ; then
  echo -e "${ERROR_PREFIX} LOCAL_RESOLVCONF_LINKNAME not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 5
  fi
elif [[ -z "${LOCAL_RESOLVCONF_TARGET}" ]] ; then
  echo -e "${ERROR_PREFIX} LOCAL_RESOLVCONF_TARGET not defined, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 6
  fi
elif [[ ! -r "${LOCAL_RESOLVCONF_TARGET}" ]] ; then
  echo -e "${ERROR_PREFIX} resolvconf file='${LOCAL_RESOLVCONF_TARGET}' not found, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 7
  fi
elif [[ ! -L '${LOCAL_RESOLVCONF_LINKNAME}' ]] ; then
  for CMD in \
    "sudo ln -sf ${LOCAL_RESOLVCONF_TARGET} ${LOCAL_RESOLVCONF_LINKNAME}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
    # TODO: test for error
  done
# otherwise do nothing!
fi

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

## show what we got

for CMD in \
  "date" \
  "ls -al ${LOCAL_RESOLVCONF_LINKNAME}" \
  "nslookup ${DNS_TEST_HOSTNAME}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
  echo # newline
done

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
