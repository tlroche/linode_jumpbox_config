source ~/my_linode_bash_library.sh

sudo aptitude update
# Following is for Debian versions >= 7 per https://www.linode.com/docs/networking/vpn/secure-communications-with-openvpn-on-ubuntu-12-04-precise-and-debian-7#tunneling-all-connections-through-the-vpn
# Packages={dnsmasq, resolvconf} needed for forwarding through the VPN (including forwarding DNS traffic)
sudo aptitude -y install openvpn dnsmasq resolvconf

sudo sh -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'
FP='/etc/sysctl.conf'
VAR='net.ipv4.ip_forward'
fgrep -nH -e ${VAR} ${FP}
sudo_backup ${FP}
# TODO: don't rely on this particular style of commenting!
sudo sed -i -e "s/^#${VAR}/${VAR}/" ${FP}
fgrep -nH -e ${VAR} ${FP}

sudo configure_iptables_to_forward.sh
sudo iptables -L

TARGET_FP='/etc/rc.local'
tail ${TARGET_FP}
sudo_backup ${TARGET_FP}
# remove `exit 0` from EOF (will replace later)
sudo sed -i -e 's/exit 0//' ${TARGET_FP}
# TODO: test for files!
SOURCE_FP="${HOME}/configure_iptables_to_forward.sh" # routing rules to append
sudo sh -c "cat ${SOURCE_FP} >> ${TARGET_FP}"
# NOTE: this must follow the routing rules, per step=13 @ https://www.linode.com/docs/networking/vpn/secure-communications-with-openvpn-on-ubuntu-12-04-precise-and-debian-7#tunneling-all-connections-through-the-vpn to fix ultra-VPN routing
SOURCE_FP="${HOME}/configure_dnsmasq_to_restart.sh" # restart dnsmasq
sudo sh -c "cat ${SOURCE_FP} >> ${TARGET_FP}"
# replace `exit 0` @ EOF
# since we're `sudo sh`, DO NOT `echo -e`: that's bash-only
sudo sh -c "echo 'exit 0' >> ${TARGET_FP}" # TODO: append that to configure_iptables_to_forward.sh?
tail ${TARGET_FP}

FP='/etc/dnsmasq.conf'
for VAR in 'listen-address' 'bind-interfaces' ; do
  fgrep -nH -e ${VAR} ${FP}
done
sudo_backup ${FP}
# TODO: don't rely on these particular styles of commenting!
VAR='listen-address'
# note values specified by step=10 in https://www.linode.com/docs/networking/vpn/secure-communications-with-openvpn-on-ubuntu-12-04-precise-and-debian-7#tunneling-all-connections-through-the-vpn to fix ultra-VPN routing
sudo sed -i -e "s/^#${VAR}=/${VAR}=127.0.0.1,10.8.0.1/" ${FP}
VAR='bind-interfaces'
sudo sed -i -e "s/^#${VAR}/${VAR}/" ${FP}
for VAR in 'listen-address' 'bind-interfaces' ; do
  fgrep -nH -e ${VAR} ${FP}
done

SOURCE_FP="${HOME}/append_to_DNS_config.txt" # has lines to append
TARGET_FP='/etc/network/interfaces'
tail ${TARGET_FP}
sudo_backup ${TARGET_FP}
# add blank line separator
# since we're `sudo sh`, DO NOT `echo -e`: that's bash-only
sudo sh -c "echo >> ${TARGET_FP}"
# add actual configuration lines
sudo sh -c "cat ${SOURCE_FP} >> ${TARGET_FP}"
tail ${TARGET_FP}
