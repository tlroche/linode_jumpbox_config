#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Get the public IP# (PUBLIC_IPN) for the current host from some site of choice, and
### just `echo` (to stdout/1--everything else should go to stderr/2) the detected IP#,
### for assignment to variable of choice: here, probably
### * CLIENT_PUBLIC_IPN (the client/laptop's ISP-assigned public IP#)
### * MYNODE_PUBLIC_IPN (the OpenVPN server's linode-assigned public IP#)
### * whatever the F5VPN assigns (if anything)

### Requires:
### * a working website or webservice (aka 'getter' below) for retrieving public IP#
### * elinks==text-based web browser. TODO: find webservice for IP# getting
### * basename, dirname
### * fgrep
### * nslookup
### * readlink
### * sed
### * source (TODO: set outside of script)
### * tail
### * tr

### Requires path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
### Presuming you checked out the project to "${HOME}/code/bash/linode/linode_jumpbox_config",
### you could pass the path like:
### > DIR="${HOME}/code/bash/linode/linode_jumpbox_config/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" source "${DIR}/get_public_IP_n.sh"

### TODO:
### * cleaner way to extend code from IP_GETTER_* (defined in public.properties)

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}
#echo -e "get_public_IP_n.sh: THIS='${THIS}'"

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='get_public_IP_n.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

## other needed properties must be set here, which must be set by caller
if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### following code works for whatismyip.com ... except that they limit accesses/day
### ----------------------------------------------------------------------

# ## IPN==dotted-quad IPv4 "IP number" , not really needed here but will use later
# IP_GETTER_IPN="$(nslookup "${IP_GETTER_FQDN}" | fgrep -A 1 "${IP_GETTER_FQDN}" | tail -n 1 | sed -e 's/Address: //')"

# ## payload
# PUBLIC_IPN="$(elinks -dump "${IP_GETTER_URI}" | fgrep -A 1 -e 'Your IP' | tail -n 1 | tr -d ' ')"
# if [[ -z "${PUBLIC_IPN}" ]] ; then
#   echo -e "${ERROR_PREFIX} could not retrieve public IP# PUBLIC_IPN, exiting ..." 1>&2
#   # not for `source`ing
#   if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
#     exit 4
#   fi
# else
##   echo -e "${MESSAGE_PREFIX} client IP# seen by ${IP_GETTER_FQDN}==${PUBLIC_IPN}"
#   echo -e "${PUBLIC_IPN}" # retval
# fi

### ----------------------------------------------------------------------
### following code works for api.hostip.info
### ----------------------------------------------------------------------

## IPN==dotted-quad IPv4 "IP number" , not really needed here but will use later
IP_GETTER_IPN="$(nslookup "${IP_GETTER_FQDN}" | fgrep -A 1 "${IP_GETTER_FQDN}" | tail -n 1 | sed -e 's/Address: //')"

## payload
PUBLIC_IP_INFO="$(elinks -dump "${IP_GETTER_URI}")"
if [[ -z "${PUBLIC_IP_INFO}" ]] ; then
  echo -e "${ERROR_PREFIX} could not retrieve public IP info from '${IP_GETTER_URI}', exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 5
  fi
else

  PUBLIC_IPN="${PUBLIC_IP_INFO##*IP: }"
  if [[ -z "${PUBLIC_IPN}" ]] ; then
    echo -e "${ERROR_PREFIX} could not retrieve public IP# from '${IP_GETTER_URI}', exiting ..." 1>&2
    # not for `source`ing
    if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
      exit 6
    fi
  else
#    echo -e "${MESSAGE_PREFIX} client IP# seen by ${IP_GETTER_FQDN}==${PUBLIC_IPN}"
    echo -e "${PUBLIC_IPN}" # retval
  fi

fi # [[ -z "${PUBLIC_IP_INFO}" ]]

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
