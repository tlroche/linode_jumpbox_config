#!/usr/bin/env bash

### Part of project=linode_jumpbox_config , see https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home

### Crudely test (from client device) to see if OpenVPN server is running on jumpbox:
### https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home#rst-header-client-device
### https://bitbucket.org/tlroche/linode_jumpbox_config/wiki/Home#rst-header-jumpbox

### Requires:
### * path to private.properties=PRIVATE_PROPERTIES_FP set in parent environment.
###   Presuming you checked out the project to "${HOME}/code/bash/linode/linode_jumpbox_config",
###   you could pass the path like:
###   > DIR="${HOME}/code/bash/linode/linode_jumpbox_config/scripts" ; PRIVATE_PROPERTIES_FP="${DIR}/private.properties" source "${DIR}/get_private_IP_n.sh"
### * path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment (ditto)
### * basename, dirname, readlink (just for logging)
### * whatever you use in public.properties:OPENVPN_PROCESS_QUERY_CMD
### * source (TODO: set outside of script)

### TODO: check error/return codes from test commands!

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}

if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
  THIS_FN='check_OpenVPN_server.sh'
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

### ----------------------------------------------------------------------
### get properties
### ----------------------------------------------------------------------

if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

if   [[ -z "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PRIVATE_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PRIVATE_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PRIVATE_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### payload
### ----------------------------------------------------------------------

if   [[ -z "${MYNODE_USER_NAME}" ]] ; then
  echo -e "${ERROR_PREFIX} could not retrieve MYNODE_USER_NAME, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 4
  fi
elif [[ -z "${MYNODE_IPV4}" ]] ; then
  echo -e "${ERROR_PREFIX} could not retrieve MYNODE_IPV4, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 5
  fi
elif [[ -z "${MYNODE_USER_SHELL_CMD}" ]] ; then
  echo -e "${ERROR_PREFIX} could not retrieve MYNODE_USER_SHELL_CMD, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 6
  fi
elif [[ -z "${OPENVPN_PROCESS_QUERY_CMD}" ]] ; then
  echo -e "${ERROR_PREFIX} could not retrieve OPENVPN_PROCESS_QUERY_CMD, exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 7
  fi
else

  ## query OpenVPN process from client
  OPENVPN_SERVER_PROCESS_QUERY_CMD="${MYNODE_USER_SHELL_CMD} '${OPENVPN_PROCESS_QUERY_CMD}' 2> /dev/null"
  for CMD in \
    'date' \
    "${OPENVPN_SERVER_PROCESS_QUERY_CMD}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
    ## TODO: test error/return code
  done
  echo # newline
fi
