### Configure `iptables` to forward traffic through a linode. This automates step 4 of the linode doc
### https://www.linode.com/docs/networking/vpn/secure-communications-with-openvpn-on-ubuntu-12-04-precise-and-debian-7#tunneling-all-connections-through-the-vpn
### from which the following lines are copied.

### Contents of this file will also be used to automate step 5 in same doc (i.e., append to /etc/rc.local), so DO NOT

### - prepend with `#!/bin/bash`

### - add any logging or dependencies

### This script must be run as `sudo` to work.

### TODO: parameterize
### * 'eth0' from public.properties
### * '10.8.0.0/24' from get_OpenVPN_routes.sh

iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -s 10.8.0.0/24 -j ACCEPT
iptables -A FORWARD -j REJECT
iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE
iptables -A INPUT -i tun+ -j ACCEPT
iptables -A FORWARD -i tun+ -j ACCEPT
iptables -A INPUT -i tap+ -j ACCEPT
iptables -A FORWARD -i tap+ -j ACCEPT
